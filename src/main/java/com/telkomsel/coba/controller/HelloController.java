package com.telkomsel.coba.controller;

import com.telkomsel.coba.helper.RedisHelper;
import com.telkomsel.coba.model.*;
import com.telkomsel.coba.repository.redis.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.List;

@RestController
@RequestMapping("/api")
public class HelloController {

    @Autowired
    private RedisHelper redisHelper;

    @GetMapping("/redis/classroom")
    public ClassRoom save() {
        ClassRoom classRoom = new ClassRoom();
        classRoom.setId(10);
        classRoom.setName("Janatun Naim");
        classRoom.setNumber(2);
        redisHelper.save(classRoom);
        return classRoom;
    }

    @GetMapping("/redis/classroom/get")
    public ClassRoom getClass(@RequestParam String id) throws IOException {
        try {
            return (ClassRoom) redisHelper.findByKey(id,ClassRoom.class);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}

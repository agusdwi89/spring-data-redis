package com.telkomsel.coba.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.telkomsel.coba.model.RedisData;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Duration;
import java.util.LinkedHashMap;

@Repository
public class RedisHelper {

    private String KEY = "Movie";
    private RedisTemplate<String, Object> redisTemplate;
    private ValueOperations valueOperations;

    @Autowired
    public void RedisHelper(RedisTemplate<String, Object> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init(){
        valueOperations = redisTemplate.opsForValue();
    }

    private String composeHeader(String key) {
        return String.format(this.KEY+":%s", key);
    }

    public void save(RedisData redisData) {
        this.KEY = redisData.getKey();
        valueOperations.set(
                composeHeader(redisData.getId().toString()),
                redisData,
                Duration.ofSeconds(redisData.getTtl()));
    }

    public RedisData findByKey(final String id,Class<?> redisClass) throws IOException, JSONException {
        LinkedHashMap obj = (LinkedHashMap) valueOperations.get(composeHeader(id));
        if(obj != null){
            JSONObject myResponse = new JSONObject(obj.toString());
            ObjectMapper mapper = new ObjectMapper();
            RedisData map = (RedisData) mapper.readValue(myResponse.toString(),redisClass);
            return map;
        }else return null;
    }
}

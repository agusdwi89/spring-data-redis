package com.telkomsel.coba;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.ClusterTopologyRefreshOptions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisSentinelConfiguration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.util.ClassUtils;

import java.io.Serializable;
import java.util.HashSet;

@SpringBootApplication()
public class MainApplication {

    public static void main(String[] args) {
        SpringApplication.run(MainApplication.class, args);
    }

    @Bean
    LettuceConnectionFactory redisConnectionFactory(RedisProperties redisProperties) {
        if (redisProperties.getCluster() != null) {
            // configure clustering options
            ClusterClientOptions clientOptions = ClusterClientOptions.builder()
                    .topologyRefreshOptions(ClusterTopologyRefreshOptions.builder()
                            .enableAllAdaptiveRefreshTriggers()
                            .build())
                    .build();

            LettuceClientConfiguration clientConfiguration = LettuceClientConfiguration.builder()
                    .clientName(ClassUtils.getUserClass(this.getClass()).getSimpleName())
                    .clientOptions(clientOptions).build();

            RedisClusterConfiguration configuration = new RedisClusterConfiguration(redisProperties.getCluster().getNodes());
            if (redisProperties.getPassword() != null) {
                configuration.setPassword(RedisPassword.of(redisProperties.getPassword()));
            }
            return new LettuceConnectionFactory(configuration, clientConfiguration);
        } else if (redisProperties.getSentinel() != null) {
            RedisSentinelConfiguration configuration = new RedisSentinelConfiguration(redisProperties.getSentinel().getMaster(),
                    new HashSet<>(redisProperties.getSentinel().getNodes()));
            if (redisProperties.getPassword() != null) {
                configuration.setPassword(RedisPassword.of(redisProperties.getPassword()));
            }
            LettuceClientConfiguration clientConfiguration = LettuceClientConfiguration.builder()
                    .clientName(ClassUtils.getUserClass(this.getClass()).getSimpleName())
                    .build();
            return new LettuceConnectionFactory(configuration, clientConfiguration);
        } else {
            RedisStandaloneConfiguration configuration = new RedisStandaloneConfiguration(redisProperties.getHost(), redisProperties.getPort());
            if (redisProperties.getPassword() != null) {
                configuration.setPassword(RedisPassword.of(redisProperties.getPassword()));
            }
            return new LettuceConnectionFactory(configuration);
        }
    }


    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisProperties redisProperties) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory(redisProperties));
        template.setKeySerializer (new StringRedisSerializer());
        template.setHashKeySerializer (new StringRedisSerializer ());
        template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
        template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
        return template;
    }
}
package com.telkomsel.coba.repository.redis;

import com.telkomsel.coba.model.MovieModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.*;

@Repository
public class MovieRedisRepositoryImpl implements MovieRedisRepository {
    private static final String KEY = "Movie";

    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations hashOperations;
    private ValueOperations valueOperations;

    @Autowired
    public void MovieRedisRepositoryImpl(RedisTemplate<String, Object> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init(){
        hashOperations = redisTemplate.opsForHash();
        valueOperations = redisTemplate.opsForValue();
    }

    public void add(final MovieModel movie) {
        valueOperations.set(
                composeHeader(movie.getId()),
                movie,
                Duration.ofSeconds(100));
    }

    private String composeHeader(String key) {

        String a = String.format("moviek:%s", key);
        System.out.println(a);
        return a;
    }

    public void delete(final String id) {
        hashOperations.delete(KEY, id);
    }

    public LinkedHashMap findMovie(final String id){
        return (LinkedHashMap) valueOperations.get(composeHeader(id));
    }

    public List<LinkedHashMap> findMovieAll(){
        Set<byte[]> keys = redisTemplate.getConnectionFactory().getConnection().keys("*".getBytes());
        Iterator<byte[]> it = keys.iterator();
        List<LinkedHashMap> linkedHashMaps = new ArrayList<>();

        while(it.hasNext()){
            byte[] data = (byte[])it.next();
            String key = new String(data, 0, data.length);
            linkedHashMaps.add((LinkedHashMap) valueOperations.get(key));
        }
        return linkedHashMaps;
    }
}
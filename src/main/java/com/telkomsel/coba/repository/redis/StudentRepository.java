package com.telkomsel.coba.repository.redis;


import com.telkomsel.coba.model.StudentModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<StudentModel, String> {

}

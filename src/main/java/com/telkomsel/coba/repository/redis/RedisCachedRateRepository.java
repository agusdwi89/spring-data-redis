package com.telkomsel.coba.repository.redis;

import com.telkomsel.coba.model.RateRedisEntry;
import io.micrometer.core.instrument.Tags;
import lombok.NonNull;

import java.util.Optional;

public interface RedisCachedRateRepository {
    Optional<RateRedisEntry> find(String key, Tags tags);
    void put(final @NonNull RateRedisEntry rateEntry, String tags);
    String composeHeader(String key);
}

package com.telkomsel.coba.repository.redis;

import com.telkomsel.coba.model.MovieModel;

import java.util.LinkedHashMap;
import java.util.List;

public interface MovieRedisRepository {

    void add(MovieModel movie);
    void delete(String id);
    LinkedHashMap findMovie(String id);
    List<LinkedHashMap> findMovieAll();
}

package com.telkomsel.coba.repository.redis;


import com.telkomsel.coba.model.TeacherModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends CrudRepository<TeacherModel, String> {

}

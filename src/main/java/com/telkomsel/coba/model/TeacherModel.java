package com.telkomsel.coba.model;

import lombok.*;
import org.springframework.data.redis.core.RedisHash;

@Data
@Builder(builderClassName = "builder")
@AllArgsConstructor
@Getter
@Setter
@RedisHash(value = "TEACHERS_CACHE", timeToLive = 60L)
public class TeacherModel {

    private String id;
    private String name;
    private Integer age;

}


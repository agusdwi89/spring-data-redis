package com.telkomsel.coba.model;

import lombok.*;
import org.springframework.data.redis.core.RedisHash;

@Data
@Builder(builderClassName = "builder")
@AllArgsConstructor
@Getter
@Setter
@RedisHash(value = "STUDENTS_CACHE", timeToLive = 60L)
public class StudentModel {

    private String id;
    private String name;
    private Integer age;

}


package com.telkomsel.coba.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClassRoom extends RedisData{
    Integer number;
    String name;

    public ClassRoom(){
        super.setKey("CLASSROOM_CACHE");
        super.setTtl(300);
    }
}

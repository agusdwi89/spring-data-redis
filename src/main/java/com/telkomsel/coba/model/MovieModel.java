package com.telkomsel.coba.model;

import lombok.*;

import java.io.Serializable;

@Data
@Builder(builderClassName = "builder")
@AllArgsConstructor
@Getter
@Setter
public class MovieModel implements Serializable {
    private String id;
    private String name;
}

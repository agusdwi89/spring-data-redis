package com.telkomsel.coba.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class RateRedisEntry {
    private String tenantEndpointByBlock;
    private Integer expirationSeconds;
}

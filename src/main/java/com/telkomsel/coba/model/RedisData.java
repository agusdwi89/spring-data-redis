package com.telkomsel.coba.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedisData {

    Number id;
    @JsonIgnore
    String key;
    @JsonIgnore
    Integer ttl;
}

